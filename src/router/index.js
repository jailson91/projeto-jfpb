import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ListaProcesso from '../views/ListaProcesso.vue'
import CadastrarUsuario from '../views/CadastrarUsuario.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/listaProcesso',
    name: 'ListaProcesso',
    component: ListaProcesso
  },

  {
    path: '/cadastrarUsuario',
    name: 'CadastrarUsuario',
    component: CadastrarUsuario
  },
  
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }


]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
